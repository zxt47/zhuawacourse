package com.zhuawa.concurrent;

/**
 * 线程安全Demo
 */

public class ThreadSafeSample {
	public int sharedState;

	public void nonSafeAction() {
	    System.err.println("非线程安全");
		while (sharedState < 100000) {
			int former = sharedState++;
			int latter = sharedState;
			if (former != latter - 1) {
				System.out.println("Observed data race, former is " + former + ", "
						+ "latter is " + latter);
			}
		}
	}


	public void safeActionWithSync()
    {
        //class为锁,或者当前的this为锁
        synchronized (ThreadSafeSample.class)
        {
            System.err.println("同步关键字");
            while (sharedState < 100000) {
                int former = sharedState++;
                int latter = sharedState;
                if (former != latter - 1) {
                    System.out.println("Observed data race, former is " + former + ", "
                            + "latter is " + latter);
                }
            }
        }
    }

	public static void main(String[] args) throws InterruptedException {
		ThreadSafeSample sample = new ThreadSafeSample();
		Thread threadA = new Thread() {
			public void run() {
				sample.nonSafeAction();
			}
		};
		Thread threadB = new Thread() {
			public void run() {
				sample.nonSafeAction();
			}
		};
		threadA.start();
		threadB.start();
		threadA.join();
		threadB.join();
		System.out.println("---------------------------------分割线");
        sample.sharedState=0;
		threadA =new Thread(()->{
		    sample.safeActionWithSync();
        });
		threadB =new Thread(()->{
		    sample.safeActionWithSync();
        });
        threadA.start();
        threadB.start();
        threadA.join();
        threadB.join();
	}
}
