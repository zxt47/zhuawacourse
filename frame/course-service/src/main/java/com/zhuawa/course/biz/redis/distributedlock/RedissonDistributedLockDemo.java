package com.zhuawa.course.biz.redis.distributedlock;

import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.config.TransportMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

/**
 * Redisson已经支持的分布式锁，内部机制与{@link RedisDistributedLock}一致
 */
public class RedissonDistributedLockDemo {
    @Autowired
    RedissonClient redisson;
    /**
     *
     */
    public void demo()
    {

        RLock lock=redisson.getLock("My RedissonLock");
        lock.tryLock();
        //处理逻辑
        System.out.println("123");

    }


    /**
     * 注入一个Redisson
     * @return
     */
    @Bean
    RedissonClient redisson()
    {
        Config config = new Config();
        config.setTransportMode(TransportMode.EPOLL);
        config.useSingleServer().setAddress("192.168.99.100");
        return Redisson.create(config);
    }

}
