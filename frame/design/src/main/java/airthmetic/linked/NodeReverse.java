package airthmetic.linked;

/**
 * @author apple on 2019/12/22.
 * @version 1.0
 */
public class NodeReverse {
    private static Node reverse(Node head) {
        /*如果是空链或者只是单个节点的链表  将直接返回*/
        if (head == null || head.getNext() == null) {
            return head;
        }
        /*找到了最后一个   也就是5   当前head为4  reverse为5*/
        Node reverse = reverse(head.getNext());
        /* 1-->2-->3-->4-->5   变为   5-->4  1-->2-->3-->4  此时4指向5  5 也指向4*/
        head.getNext().setNext(head);
        /*4-->null    5-->4-->null  1-->2-->3-->4 */
        head.setNext(null);
        /*返回5-->4-->null*/
        return reverse;
    }

    public static void main(String[] args) {
        Node a = new Node(1);
        Node b = new Node(2);
        Node c = new Node(3);
        Node d = new Node(4);
        Node e = new Node(5);
        a.setNext(b);
        b.setNext(c);
        c.setNext(d);
        d.setNext(e);
        /*data=1-->data=2-->data=3-->data=4-->data=5-->null*/
        System.out.println(a);
        /*递归方式反转*/
        Node reverse = reverse(a);
        /*data=5-->data=4-->data=3-->data=2-->data=1-->null*/
        System.out.println(reverse);
    }
}
